﻿namespace ScrabbleMoveGenerator
{
    public interface IWordDatabase
    {
        bool Contains(string word);
        bool ContainsPrefix(string prefix);
    }
}
