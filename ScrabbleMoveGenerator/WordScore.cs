﻿namespace ScrabbleMoveGenerator
{
    public class WordScore
    {
        public WordScore(string word, int score, int start, Direction direction)
        {
            Word = word;
            Score = score;
            Direction = direction;
            Start = PotentialMove.SquareToCoords(start);
        }
        public string Word { get; }
        public int Score { get; }
        public string Start { get; }
        public Direction Direction { get; }
    }
}
