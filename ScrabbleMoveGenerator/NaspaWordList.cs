﻿using System;
using System.IO;

namespace ScrabbleMoveGenerator
{
    /// <summary>
    /// The complete NASPA word list 2020
    /// </summary>
    public class NaspaWordList : IndexedWordList
    {
        public NaspaWordList()
        {
            using (var sr = new StreamReader(typeof(NaspaWordList).Assembly.GetManifestResourceStream("ScrabbleMoveGenerator.nwl2020.txt")))
            {
                var line = sr.ReadLine();
                while (!string.IsNullOrEmpty(line))
                {
                    Add(line.Trim());
                    line = sr.ReadLine();
                }
            }
        }
    }
}
