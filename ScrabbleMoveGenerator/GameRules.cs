﻿using System.Collections.Generic;

namespace ScrabbleMoveGenerator
{
    public static class GameRules
    {
        public static readonly Dictionary<char, int> TileDistribution = new Dictionary<char, int>()
        {
            {'A', 9 },
            {'B', 2 },
            {'C', 2 },
            {'D', 4 },
            {'E', 12 },
            {'F', 2 },
            {'G', 3 },
            {'H', 2 },
            {'I', 9 },
            {'J', 1 },
            {'K', 1 },
            {'L', 4 },
            {'M', 2 },
            {'N', 6 },
            {'O', 8 },
            {'P', 2 },
            {'Q', 1 },
            {'R', 6 },
            {'S', 4 },
            {'T', 6 },
            {'U', 4 },
            {'V', 2 },
            {'W', 2 },
            {'X', 1 },
            {'Y', 2 },
            {'Z', 1 },
            {'_', 2 }
        };

        private static readonly int[] tileScore =
        {
            1,
            3,
            3,
            2,
            1,
            4,
            2,
            4,
            1,
            8,
            5,
            1,
            3,
            1,
            1,
            3,
            10,
            1,
            1,
            1,
            1,
            4,
            4,
            8,
            4,
            10,
            0
        };

        public static int TileScore(char c)
        {
            if (char.IsUpper(c))
                return tileScore[c - 65];
            return 0;
        }

        public static int? PreviousSquare(int square, Direction direction)
        {
            if (direction == Direction.Horizontal)
            {
                if (square % 15 == 0)
                    return null;
                return square - 1;
            }
            if (direction == Direction.Vertical)
            {
                if (square >= 15)
                    return square - 15;
                return null;
            }
            return null;
        }

        public static int? NextSquare(int square, Direction direction)
        {
            if (direction == Direction.Horizontal)
            {
                if (square % 15 == 14)
                    return null;
                return square + 1;
            }
            if (direction == Direction.Vertical)
            {
                if (square >= 210)
                    return null;
                return square + 15;
            }
            return null;
        }
    }
}
