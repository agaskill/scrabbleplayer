﻿using System;
using System.Linq;

namespace ScrabbleMoveGenerator
{
    public class Board
    {
        private const string initialBoard =
            "3  +   3   +  3" +
            " 2   *   *   2 " +
            "  2   + +   2  " +
            "+  2   +   2  +" +
            "    2     2    " +
            " *   *   *   * " +
            "  +   + +   +  " +
            "3  +   2   +  3" +
            "  +   + +   +  " +
            " *   *   *   * " +
            "    2     2    " +
            "+  2   +   2  +" +
            "  2   + +   2  " +
            " 2   *   *   2 " +
            "3  +   3   +  3";

        private readonly string board;
        private readonly bool isEmpty;

        public Board(char[] board)
            : this(FromCharArray(board))
        { }

        public Board(string board)
        {
            this.board = board;
            isEmpty = !board.Any(char.IsLetter);
        }

        public Board()
        {
            board = initialBoard;
            isEmpty = true;
        }

        private static string FromCharArray(char[] board)
        {
            var result = initialBoard.ToCharArray();
            for (int i = 0; i < board.Length; i++)
            {
                if (char.IsLetter(board[i]))
                {
                    result[i] = board[i];
                }
            }
            return new string(result);
        }

        public bool IsOpen(int square)
        {
            return !char.IsLetter(board[square]);
        }

        public bool IsPlayable(int square)
        {
            if (isEmpty)
                return square == 112;
            var row = Math.DivRem(square, 15, out int col);
            return (row > 0 && char.IsLetter(board[square - 15]))
                || (row < 14 && char.IsLetter(board[square + 15]))
                || (col > 0 && char.IsLetter(board[square - 1]))
                || (col < 14 && char.IsLetter(board[square + 1]));
        }

        public char this[int i] => board[i];

        public char[] ToCharArray() => board.ToCharArray();

        public override string ToString()
        {
            var sb = new System.Text.StringBuilder();
            for (int i = 0; i < 15; i++)
            {
                sb.AppendLine(
                    System.Text.RegularExpressions.Regex.Replace(
                        board.Substring(i * 15, 15),
                        @"[23*+]",
                        " "));
            }
            return sb.ToString();
        }
    }
}
