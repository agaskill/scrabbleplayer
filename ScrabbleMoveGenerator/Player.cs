﻿namespace ScrabbleMoveGenerator
{
    public class Player
    {
        public Player(string name, int score, string tiles)
        {
            Name = name;
            Score = score;
            Tiles = tiles;
        }
        public string Name { get; }
        public int Score { get; }
        public string Tiles { get; }

        public Player WithTiles(string tiles)
        {
            return new Player(Name, Score, tiles);
        }
    }
}
