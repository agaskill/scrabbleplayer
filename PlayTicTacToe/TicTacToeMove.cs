﻿namespace PlayTicTacToe
{
    public class TicTacToeMove
    {
        public TicTacToeMove(int square, char mark)
        {
            Square = square;
            Mark = mark;
        }

        public int Square { get; }
        public char Mark { get; }

        public override string ToString()
        {
            return $"{Mark} on {Square}";
        }
    }
}
