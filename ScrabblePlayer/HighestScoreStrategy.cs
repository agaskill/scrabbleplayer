﻿using ScrabbleMoveGenerator;
using System.Linq;

namespace ScrabblePlayer
{
    public class HighestScoreStrategy : IStrategy
    {
        public PotentialMove ChooseMove(GameState state, IWordDatabase wordlist)
        {
            var gen = new MoveGenerator(state.Board, wordlist);
            return gen.FindMoves(state.CurrentPlayer.Tiles.ToCharArray()).FirstOrDefault();
        }
    }
}
