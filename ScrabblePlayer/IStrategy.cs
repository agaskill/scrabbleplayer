﻿using ScrabbleMoveGenerator;

namespace ScrabblePlayer
{
    public interface IStrategy
    {
        PotentialMove ChooseMove(GameState state, IWordDatabase wordlist);
    }
}
